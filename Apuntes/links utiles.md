### REDES DE PC II
#### [Libro 8va edición](https://github.com/armatrix/cs-computer-networking)
#### [Biblioteca USM Libro](https://bibliotecadigital.usm.cl/info/computer-networking-00566510)

### Seminario
#### [You and Your Research. Richard Hamming.](https://www.youtube.com/watch?v=a1zDuOPkMSw)
[Transcripcciones](http://profesores.elo.utfsm.cl/~agv/ipd438/papers/YouAndYourResearch.html) (user: ipd438, password: 438ipd) 
#### [LEADERSHIP LAB: The Craft of Writing Effectively](https://www.youtube.com/watch?time_continue=1&v=vtIzMaLkCaM&feature=emb_title)
#### [Federico Mayor Zaragoza: "La Universidad, pieza clave para un futuro más justo":](https://www.youtube.com/watch?v=BrGUfW-3Rqw)
#### [Randy Pausch reprising his "Last Lecture":](https://www.youtube.com/watch?v=p1CEhH5gnvg)
#### [Una buena idea no es suficiente, según Allen Newell:](http://profesores.elo.utfsm.cl/~agv/elo323.ipd438/2s19/lectures/Newell.html)
#### [Non-Technical Talks by David Patterson, U.C. Berkeley:](https://people.eecs.berkeley.edu/~pattrsn/talks/nontech.html) 

### Bases de Datos:
[Biblioteca USM](https://biblioteca.usm.cl/electronica/bases_de_datos)
[Scitepress](https://www.scitepress.org/HomePage.aspx)
[Estado técnico](http://profesores.elo.utfsm.cl/~agv/research/propiedadIndustrial/Busquedas_Estado_Tecnica.html)
[Conferencias](http://profesores.elo.utfsm.cl/~agv/elo323.ipd438/Conferences.html)
