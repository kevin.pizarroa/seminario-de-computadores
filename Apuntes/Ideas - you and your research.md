## Ideas sobre charla: You and your research - Richard Hamming**

### Intro:
En esta charla, el profesor y científico Richard Hamming nos presenta diversos puntos de vista sobre cómo ser un buen investigador, exponiendo así el porqué sólo algunos académicos logran realizar aportes significativos. 

### Desarrollo:

- Sus primeros pensamientos comienzan desde la envidia, las ansias de saber en qué lo diferenciaba de los grandes investigadores como Fermi, Feynman y tantos más.
- Es de gran importancia la charla ya que sólo tenemos una vida. Incluso si tuviesemos varias, ¿por qué no hacer cosas significativas en cada una?

#### Sobre la suerte. 

¿La investigación es suerte? Hamming nos plantea que si y no...
Se trata de repetición y por ello cita a Pasteur: La suerte favorece a las mentes preparadas. Puedes tener suerte y no hacer nada con ella, no vale nada. Diferente es tener suerte y sacarle el provecho al máximo, estar preparados para poder desarrollar las ideas. Cita a Newton: ''Si los demás pudieran pensar tanto como yo, entonces podrían tener resultados similares.''Presenta el caso de Einstein y sus pensamientos prematuros sobre la luz, luego en su adultez con más conocimientos retrocedió a esos pensamientos para desarrollar la relatividad especial.

#### Sobre la determinación

Una característica esencial de los académicos existosos es la determinación. Si tu crees que puedes resolver problemas importantes entonces TU PODRÁS. Característica muy presente en Shannon.

#### Sobre la edad. 

Usualmente, para la ciencia se prioriza a los jóvenes mientras que en otras áreas (política o música) se prioriza a los de más edad.

#### Sobre la fama. 

Una vez te haces famoso la gente sóolo suele trabajar en "grandes problemas". Se enfrascan en replicar su gran éxito y no es así como funciona, tal como su último trabajo, estas cosas no se pueden forzar.

#### Sobre las condiciones de trabajo.

La gente trabaja mejor cuando está en peores condiciones de trabajo. Cito (yo KP) a un expositor del IEEE en la asignatura de Proyectos electrónicos: ''cuando un emprendedor tiene en juego el pan de cada día o la renta de su casa es cuando hace los mejores trabajos.'' (respecto a la innovación)

#### Sobre el tiempo.

Interpretación a palabras de Bode: ''El conocimiento y la productividad son como el interés compuesto.'' Basta con esforzarse una hora más cada día por 1 año para superar a alguien con la misma habilidad (no es raro). ''No me gusta decirlo en frente de mi esposa, pero la rechacé en ocasiones. Necesitaba estudiar''

#### Sobre la ambiguedad. 

Cito a Hamming: ''Los grandes académicos tienen alta tolerancia a la ambiguedad.'' Cuestiona todo lo existente, no dar nada por hecho. Creer, pero no a ciegas.

- "Emotional commitment". Trabajar a diario en un problema hace que, en algún momento, el subconciente también comience a trabajar en el problema. De esta misma forma en los sueños también se comenzará a trabajar en ello. Luego, un día simplemente puedes despertar con la respuesta ahí, gratis. "deeply  committed".
- Buscar nuevas fuentes de información incluso en los almuerzos! juntarse con químicos, físicos... Variar el entorno es lo importante, conocer sus trabajos y los problemas interesantes en sus áreas.

#### Sobre elección del tema.

- Puedes responderte a ti mismo la pregunta: ¿Cuáles son los problemas importantes en mi área? Si no puedes trabajar en problemas importantes entonces es poco probable que vayas a tener resultados importantes.
- Un problema es importante/interesante cuando SABES cómo atacarlo/abordarlo.

- Aprovecha TODAS tus oportunidades. Deja lo demás de lado cuando se te presenta un problema interesante.
- Correlación entre la gente que trabaja con la puerta abierta y ser abiertas de mente. De esta manera son más interrumpidos pero también sabrán qué tipos de problemas interesantes están esperando allí afuera.
- A veces, basta con darle una pequeña vuelta a los problemas que estamos trabajando, no debes cerrarte a un problema al 100%
- Mira a lo grande. Buscar un buen problema e idear estrategias para abordarlo, puedes segmentarlo en partes para conquistarlo. La ciencia es un trabajo acumulativo, por lo cual tu trabajo debe aportar a ello, que los demás puedan posarse en tu trabajo para generar más CIENCIA.
- Debes ser un investigador a tiempo completo, estar en cada momento y con cada parte de ti en pos de la investigación. Pero no sólo basta con hacer un buen trabajo, también hay que saber como ''venderlo'' y hacer que la gente piense ''sí, es un buen trabajo''. Es vital que la gente pare y lea tu trabajo.

#### Saber VENDER tu trabajo.

1. Saber como escribir claro y bien.
2. Saber como hacer charlas formales.
3. Saber como hacer charlas informales.

#### Consejos para las charlas
- Ser tan general como sea posible, que la audiencia pueda seguirte.
- Dar pistas y el trasfondo del por qué este tópico es importante.
- Saber establecer límites con las demás personas. Tú mejor que nadie sabes cuales son las limitaciones para obtener resultados, en cuanto a tiempo y recursos (computacionales, financieros, etc).
- Agradece a quienes aportaron en tu trabajo, dales el reconocimiento que merecen y hazlo saber a tus jefes (si tienes).
- Es completamente NECESARIO tener un compromiso real para tener un trabajo de primera clase, no basta sólo con tener las habilidades.
- Sobre la personalidad del investigador. No se puede tener el control sobre todo, hay que saber cómo funciona el ''sistema'' para luego trabajar con el favor del sistema. Si quieres hacer algo, hazlo y no preguntes. Es mejor presentar algo a que pedir si hacerlo. El ego es un enemigo fundamental, a veces hay que desertar a algunos malos hábitos. Ejemplo de la ropa: Vestir como gusto o vestir para poder ser ''bien recibido''. Vestir de acuerdo a las expectativas de la audiencia.
- Usa tu energía eficientemente. Hay dos tipos de personas, las que utilizan el sistema a su favor para hacer ciencia de primera clase y aquellas que luchan contra el sistem. Son muy pocas las personas que pueden hacer ambas, demanda mucha energía.
- Ver el lado positivo de las cosas. Ser capaz de darle vuelta a las situaciones dificiles, cambiando el rumbo, la herramienta o lo que sea necesario.
- Para ser un investigador de primer clase necesitas conocerte, conocer tus fortalezas y debilidades. Cómo puedes convertir tus debilidades en una virtud.

#### Resumen: Por qué mucha gente no son exitosas...

1.	No trabajan en problemas importantes.
2.	No se involucran emocionalmente.
3.	No resuelven las cosas que les son fáciles, que son dificiles para otros pero igualmente importantes.
4.	No se engañen a si mismos (coartadas=alibis).

#### Q&A 
1. Saber lidiar con el estrés, te estás involucrando emocionalmente con tu trabajo, será inevitable.
2. Para administrar es recomendable observar a los demás.
3. No recomienda el brainstorming.
4. Lee, pero no es la cantidad de libros o articulos que lees lo importante sino que es cómo lo lees. Convérsalo, da nuevas perspectivas.
5. Haz buenos trabajos. Hamming window (Hamming + Tukey)
6. Impacto a corto plazo: papers. Impacto a largo plazo: libros. Pero todo es necesario: papers, charlas, libros...
7. Puedes cambiar de área. Cada 7 años app. Tendrás que empezar desde 0.
8. Debes elegir, entre administración o investigación. Tienes que ser claro en lo que quieres.
9. Rodearse de grandes personas, pero también convertirte en una gran persona. Así poder aprender de ellos y ellos de ti.

---

#### Frases destacadas:

1. "There are wavelengths that people cannot see, there are sounds that people cannot hear, and maybe computers have thoughts that people cannot think."
2. "Creativity comes out of your subconscious."
3. "It is a poor workman who blames his tools - the good man gets on with the job, given what he's got, and gets the best answer he can.""

---

* Esfuerzo
* Aprovechar las oportunidades
* Abierto a los cambios necesarios
* No perder tiempo en cosas no razonables
* Condiciones personales e investigacion
* Usar instancias cualesquieras para seguir investigando (almuerzo)