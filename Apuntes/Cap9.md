<script
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
  type="text/javascript">
</script>

## C1 - Resumen: Redes de Computadores: Un enfoque descendente.
### Capítulo 9 Multimedia en Redes de Computadores. 7ma edición
#### Capítulo 9.1 Aplicaciones multimedia en redes
##### Contexto
El video streaming es el mayor consumidor de ancho de banda en Internet.

* Netflix y Youtube poseen el 16\% del tráfico ISP residencial de bajada.
* Hay cerca de 1 billón de usuarios YouTube y cerca de 75 millones de usuarios en Netflix &rarr; Problemas de escalabilidad.
* Diferentes usuarios tienen diferentes capacidades. Diferentes tasas de transmisión.

**Solución: una infraestructura distribuida a nivel de aplicación.**

***Quality of Service*** (QoS): La red provee a las aplicaciones con nivel de desempeño requerido para su funcionamiento.

---
##### Características fundamentales de las aplicaciones multimedia en red

* Son sensibles a los retardos.
	* Retardo extremo a extremo
	* Variaciones de retardo (jitter)
* Tolerante a pérdidas: pérdidas no frecuentes causan distorsiones menores.
* Antítesis de datos, los cuales son intolerante a pérdidas pero tolerante a retardo.

La señal analógica de audio es muestrada a tasa constante. Para la telefonía es cercano a los **8000 muestras/s**, mientras que para la música en CD es cercana a las **44100 muestras/s**. Luego cada muestra es cuantizada, por ejemplo con 8 bits (256 valores/niveles). Así para telefonía con 8 bits se tienen tasas de **64.000 bps** (bits per second)

Para el video es parecido, se desplegan imágenes a tasa constante, por ejemplo 24 imagenes/segundo. Además se sabe que cada imagen es un arreglo de pixeles, donde cada pixel es representado por una codificación en \# bits. Existiendo así dos codificaciones:

- Codificación espacial: En lugar de enviar cada uno de los pixeles, si estos se repiten entonces mandamos la cantidad de veces que se repiten. Por ejemplo, en imagenes binarias mandar algo del estilo: 5B,8N,2B... (B: blanco y N: negro).
- Codificación temporal: Aquí no mandamos el frame i+1 completo, sino que la diferencia con respecto al frame anterior (i).

---

##### Clases de aplicaciones multimedia en red

- Streaming de audio y video almacenado.
	- **Streaming:** Puede ser reproducido antes de la descarga del archivo completo.
	- **Almacenado (en servidor):** Puede transmitir más rápido que la reproducción del audio/video. (implica el almacenamiento/buffer en cliente)
- Streaming de audio y video en vivo.
- Voz/video conversacional sobre IP.

#### Capítulo 9.2 Streaming de video almacenado

El medio está almacenado en la fuente y es transmitido al cliente. Por lo cual el streaming se define como *la reproducción en cliente comienza antes que todos los datos han llegado*.

![streaming](Imagenes/streaming.png "streaming video almacenado")

Dentro de los grandes desafios en el streaming de video se encuentran:

- La **restricción de reproducción continua**. Debemos tener en cuenta que la red es variable (jitter), por lo cual se necesita un **buffer** del lado del cliente para cumplir este requerimiento.
- **Interactividad del cliente**: pause, fast-forward, rewind, jump, etc.
- **Paquetes se pueden perder** y deben ser retransmitidos. ¿Cómo nos aseguramos de la secuencialidad?

El buffer del lado del cliente no es trivial, se debe tener en consideración las tasas de transmisión y generar los retardos adecuados para cada evento.

---

##### Streaming multimedia: UDP

UDP posee las siguientes características para el streaming de multimedia:

* El servidor envía a tasa apropiada para el cliente
	* A menudo: tasa envío = tasa codificación = tasa constante
	* Tasa de transmisión puede ser inconsciente de nivel de congestión
* Retardo de reproducción puede ser corto (2 a 5 segundos) para remover jitter de la red.
* Recuperación de errores sólo a nivel de aplicacióno, si lo permite el tiempo.
* Real-Time Protocol (RTSP) [RFC 3550](https://www.rfc-editor.org/rfc/rfc3550.html) define tipos de carga multimedia.
* Desventaja: UDP podría no pasar a través de cortafuegos.

---

##### Streaming multimedia: HTTP

HTTP posee las siguientes características para el streaming de multimedia:

* El archivo multimedia es obtenido a través de **GET**, **HTTP**.
* El envío a tasa máxima depende de TCP.
* Tasa de llenado del buffer fluctúa debido al control de congestión de TCP.
* Hay mayores retardos de reproducción: para suavizar la tasa de entrega de TCP.
* Ventaja: HTTP/TCP pasa más facilmente a través del cortafuegos.

---

##### Streaming multimedia: DASH
DASH funciona a través de HTTP, viene a ''mejorarlo''.

* Servidor:
	* **Divide** el archivo de video en múltiples trozos.
	* Cada trozo es almacenado y **codificado a diferentes tasas**.
	* **Archivo manifiesto**: provee URLs para los diferentes trozos.
* Cliente:
	* Mide periódicamente la tasa de transmisión.
	* Usando el manifiesto, requiere de un trozo a la vez en dependencia de la tasa de transmisión actual.

Por otro lado se utiliza además tecnologías complementarias para potenciar DASH, tales como CDN y DNS.

#### Capítulo 9.3 Voice-over-IP (VoIP)

Para este servicio se deben cumplir un par de requerimientros extremo-extremo mínimos para una conversación:

* Retardos menores a 150 msec
* Se debe incluir los retardos de aplicación (empacar y reproducir) y de la red.

---

##### Características de VoIP

* Se **agrega un encabezado** en la capa de aplicación para cada segmento (protocolo RTP).
* Segmento de habla + encabezado es encapsulado en datagrama de UDP o segmento de TCP.
* La aplicación envía datagrama UDP por el socket cada 20 ms durante el habla.
* Las pérdidas en la red suelen ser debido a congestión en la red, también debido a los enlaces con mayor BER (Bit Error Rate, ej. enlaces inalámbricos).
* Las pérdidas por retardo suelen ser debido a que el datagrama IP llega tarde para su reproducción. El retardo máximo tolerable típico es de 400 ms.
* La tolerancia a pérdidas típica suele ser entre 1\% a 10\% para codificación de voz.

![jitter](Imagenes/jitter.png "delay jitter")

Para el retardo se tiene un compromiso entre el cuanto se espera y cuanta pérdida estamos dispuestos a pagar. Un retardo alto &rarr; menor pérdida, mientras que un menor retardo &rarr; mayor pérdida. 

Podemos optar por estrategias de retardo fijo y estrategias de retardo adaptativo, la primera opción no es muy buena.

---

##### Retardo de reproducción adaptativo

La idea es minimizar el retardo de reproducción, manteniendo una baja tasa de pérdida. Para ello se **estima el retardo de red**, luego se **ajusta** el retardo de reproducción.

$$ d_i = (1-u) d_{i-1} + u(r_i-t_i) $$

* $$d_i: \text{retardo promedio estimado después de recibir el i-ésimo paquete.}$$ 
* $$r_i: \text{tiempo de recepción del paquete.}$$ 
* $$t_i: \text{marca de tiempo del i-ésimo paquete.}$$ 
* $$r_i - t_i: \text{retardo de red para el i-ésimo paquete.}$$
* $$\text{u es una constante (u = 0.01)}$$

También necesitamos **estimar el promedio de las variaciones de retardo**, \\(v_i\\): 

$$v_i = (1-u)v_{i-1} + u |r_i - t_i - d_i|$$

Los estimadores \\(d_i\\) y \\(v_i\\) son calculados para cada paquete recibido.

El primer paquete de un segmento de habla es reproducido en tiempo \\(p_i\\):

$$p_i = t_i + d_i + K v_i$$

Donde K es una constante positiva. Si la reproducción es bajo demanda entonces se usa mayores valor de K.

Los ajustes que se requieran se realizarán durante los silencio.

**Para saber si un paquete es primero en un segmento de habla debe revisar las marcas de tiempo y su número de secuencia. (segmentos cada 20 ms por ejemplo).**

---

##### Recuperación de pérdidas de paquetes

Cada ACK/NAK toma más o menos 1 RTT, en ocasiones puede ser mucho. Por lo cual hay una alternativa llamada **forward error correction** (FEC), enviar suficientes bits para permitir la recuperación sin retransmisión.

\\(1^{er}\\) Esquema de FEC:

* Por cada n paquetes insertar un paquete redundante dando paridad a los n originales.
* Envía n+1 paquetes, aumentando el ancho de banda ocupado en 1/n.
* Se puede reconstruir los n paquetes originales si hay a lo más un paquete perdido de los n+1.

\\(2^{do}\\) Esquema de FEC:

* Agrega un flujo de baja calidad.
* Envia flujo de baja resolución con información redundante.
* Cuando no hay pérdidas consecutivas, el receptor puede subsanar la pérdida.
* Se podría agregar también las tramas de baja calidad (n-1) y (n-2) para tolerar pérdidas consecutivas

\\(3^{er}\\) Esquema de FEC:

Utiliza entrelazado.

* Las tramas son subdivididas en pequeñas unidades.
* Luego se entrelazan los trozos.
* Si se pierden porciones entonces no hay tanto impacto, pues se tiene la mayoría de cada tramo.

Como ventaja: no hay redundancia. Como desventaja hay retardo de reproducción.

![interleaved](Imagenes/interleaved.png "entrelazado")

---

##### Caso de estudio: VoIP en Skype

Tiene componentes P2P.

* Clientes (SC): son los pares skype comunicadios vía llamado VoIP.
* Super Nodes (SN): pares skype con funciones especiales.
* Overlay network: entre SNs para localizar SCs.
* Login server.

La operación de Skype se resume en:

1. El cliente ingresa a la red contactando a un SN usando TCP.
2. Luego existe un log-in al sevidor central de Skype.
3. Obtiene una IP de llamado desde SN, SN overlay.
4. Inicia llamada directamente al llamado.

**Problema!** Al utilizar NATs se previene el inicio de conexiones internas, por lo cual se debe buscar una solución.

**Solución:** Utilizar relay, así se realiza la conexión a través de los SNs.

#### Capítulo 9.4 Protocolos de aplicaciones de conversaciones tiempo-real

##### Real-Time Protocol (RTP)

El protocolo RTP especifica una estructura de paquete para transportar datos de audio y video, también otros medios. Este protocolo nos provee de: identificación del **tipo de carga**, **número de secuencia del paquete** y **marcas de tiempo** (time stamping). RTP corre en los **sistemas extremos**, de esta forma los paquetes RTP son encapsulados típicamente en datagramas **UDP**. Además, el protocolo permite la **interoperabilidad**.

Existen también las bibliotecas RTP, las cuales proveen una interfaz con la capa de transporte, extendiendo UDP (socket) con la información descrita anteriormente.

***Ojo que RTP no provee ningún mecanismo de entrega a tiempo de datos ni otras garantías de calidad. Servicio best-effort.***

El encabezado de RTP se compone por:

* Tipo de carga (7 bits)
	* Tipo de carga 0: PCM mu-law, 64 kbps
	* Tipo de carga 3: GSM, 13 kbps
	* Tipo de carga 7: LPC, 2.4 kbps
	* Tipo de carga 26: Motion JPEG
	* Tipo de carga 31: H.261
	* Tipo de carga 33: MPEG2 video
* Número de secuencia (16 bits). Incrementa en 1 por cada paquete RTP enviado. Usado típicamente para la **detección de pérdida de paquetes**.
* Marca de tiempo (32 bits). Refleja el instante de muestreo del primer byte en el paquete RTP.
	* Para audio aumenta en uno por cada periodo de muestreo.
	* Con habla se agrupan las muestras codificadas, para el silencio continua el incremento.
* Campo **SSRC** (Synchronization source, 32 bits). Identifica la fuente del flujo RTP. Cada usuario de una sesión RTP debe tener un SSRC distinto (**principio de unicidad**).
* Campos miscelaneos... 

---

##### Real-Time Control Protocol (RTCP)

Este protocolo trabaja en conjunto a RTP.

---

##### Session Initiation Protocol (SIP)

Puedes utilizar el servicio (Whatsapp, por ejemplo) desde varios dispositivos. Una de las grandes ventajas es que se puede ubicar a la persona no importando que dispositivo IP usa.

**Establecimiento de la llamada IP**
1. Mensaje de invitación.
2. Mensaje de confirmación (200 OK), con información y la codificación preferida.
3. ACK
4. Intercambio de mensajes de audio.

![SIP](Imagenes/sip.png "SIP")

***Mensajes SIP pueden ser TCP o UDP.***

***Puerto por omisión de SIP es 5060.***

Pueden haber otras opciones de respuesta a la invitación, como *not acceptable* en caso de no tener el codificador o simplemente rechazar la llamada.

***SIP tiene una sintaxis similar a HTTP, llamado session description protocol (SDP).***

**Servidores SIP poseen:**
* Servidor de registro SIP. Tipo de base de datos, parecido a telefonía.
* Servidor proxy SIP. Informa al servidor de registro. Es el responsable del ruteo del mensaje SIP. **Análogo a servidor local DNS más el establecimiento de conexión TCP**

***Actualmente, el protocolo SIP es preferido por sobre H.323.***

#### Capítulo 9.5 Soporte de red para multimedia

![Tabla comparativa soporte de red](Imagenes/soporte.png "Tabla comparativa soporte de red")

Granularidad, cómo puedo tratar a cada paquete que pasa por el router, todos los paquetes son tratados iguales o son diferenciados por clase.

Garantía hace referencia a garantías de envíos de datos, velocidades y otros.

---

##### "Best effort"

Tiene un alto costo de ancho de banda, por lo cual es crítico decidir cuánto bandwidth es suficiente o estimar la demanda del tráfico. Una de las grandes ventajas es la baja complejidad de los mecanismos de la red.

##### Múltiples clases de servicio

Hacer servicio best effort para cada una de las clases. Utiliza el campo de ToS para la diferenciación.

1. Se marca cada paquete para que tengan un trato diferenciado.
2. Proveer protección a una clase de las otras. No usar más de lo comprometido.
3. Es deseable usar los recursos tan eficientemente como sea posible. (analogía con pistas reservadas para el transantiago)

---

##### Mecanismos de itineración de paquetes y políticas

**Itineración**

* First come first serverd (FCFS) / First in First out (FIFO).
		* Políticas de descarte si hay buffer lleno:
				* Tail drop
				* Priority
				* Random
* Prioridad de clases. Ojo con la inanición.
* Round robin. Cíclicamente se barren las colas de cada clase.
* Weighted fair queueing (WFQ). Es un round robin ponderado.

**Políticas**

### GLOSARIO

1. TCP: Transfer Control Protocol, protocolo a nivel de capa de transporte. [RFC 793](https://www.rfc-editor.org/rfc/rfc793)
2. HTTP: Hypertext Transfer Protocol, protocolo a nivel de capa de aplicación. [RFC 2616](https://www.rfc-editor.org/rfc/rfc2616)
3. UDP: User Datagram Protocol, protocolo a nivel de capa de transporte. [RFC 768](https://www.rfc-editor.org/rfc/rfc768)
4. DASH: Dynamic, Adaptive Streaming over HTTP. [RFC 7933](https://www.rfc-editor.org/rfc/rfc7933.html)
5. CDN: Content Delivery Networks.
6. DNS: Domain Name System. [RFC 1034](https://www.rfc-editor.org/rfc/rfc1034)
7. VoIP: Voice-over-IP. [RFC 6405](https://www.rfc-editor.org/rfc/rfc6405.html)
8. RTT: Round-trip Time, tiempo de ida y vuelta de un paquete.
9. P2P: Peer-to-peer. [RFC 5694](https://www.rfc-editor.org/rfc/rfc5694.html)
10. RTP: Real-Time Protocol, protocolo parte de la capa de aplicación. [RFC 3550](https://www.rfc-editor.org/rfc/rfc3550.html). [What is RTP?](https://www.spiceworks.com/tech/networking/articles/what-is-rtp/).
11. QoS: Quality of Service. Calidad de servicio, hace referencia al trato diferenciado, contraparte de la filosofía best effort.
12. ToS: Type of Service. Es un campo del encabezado de IPv4, puede indicar la clase para poder tener QoS.