<script
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
  type="text/javascript">
</script>

## C1 - Resumen: Redes de Computadores: Un enfoque descendente.
### Capítulo 7 Redes móbiles e inalámbricas. 8va edición
#### Capítulo 7.1. Introducción
Dentro de las redes inalámbricas es posible identificar varios elementos clave, entre estos se encuentra:

- Hosts inalámbricos: Dispositivos que actúan como sistemas terminales y que ejecutan las aplicaciones. EJ: pc, tablet, etc.
- Enlaces inalámbricos: Son aquellos dispositivos que conectan a una estación base u otro host inlámbrico a través de un enlace de comunicaciones inalámbrico. En ocasiones también se utilizan dentro de una misma red para conectar entre sí routers, switches y otros equipos de red.

- Estación base: Es aquella responsable de enviar y recibir datos. A menudo será responsable de coordinar la transmisión de múltiples hosts inalámbricos que estén asociados con ella.

Usualmente se dice que los hosts asociados con una estación base operan en *modo de infraestructura*, dado que todos los servivios de red tradicionales son proporcionados por la red con la que un host se conecta a través de la estación base.

- Infraestructura de red: Es la red de mayor tamaño con la que un host inalámbrico puede querer comunicarse.

Tipos de red:

+ ***Redes basadas en infraestructura y un único salto***: Poseen una estación base conectada a una red cableada de mayor tamaño. Toda comunicación se realiza con un único salto inalámbrico. Ej: 4GLTE, institutos educacionales, cafeterías.

+ ***Redes sin infraestructura y un único salto***: En estas no existe una estación base conectada a una red inalámbrica. SIn embargo, uno de los nodos puede coordinar las transmisiones de los restantes nodos. Ej: Bluetooth.

+ ***Redes basadas en infraestructuray múltiples saltos***: En esta es la estación base la que está cableada a la red de mayor tamaño. Algunos nodos inalámbricos pueden tener que retransmitir sus comunicaciones a través de otros nodos inalámbricos. Ej: redes de sensores inalámbricos.

+ ***Redes sin infraestructura y múltiples saltos***: Es estas no existe una estación base y los nodos pueden tener que retransmitir sus mensajes a través de otros diversos nodos para alcanzar un cierto destino. Ej: MANETs y VANETs.


#### Capítulo 7.2. Características de las redes y enlaces inalámbricos
Existen varias distinciones relevantes entre los enlaces inalámbricos y los enlaces cableados, a continuación se presentan algunas de estas:
- ***Intensidad decreciente de la señal***: La radiacioón electromagnética se atenúa a medida que va atravesando la materia. Esto se denomina *pérdida de propagación* y se genera al incrementar la distancia entre el emisor y el receptor.

- ***Interferencia de otros orígenes***: Las fuentes de radio señales que transmiten en la misma banda de frecuencia interferirán entre si.

- ***Propagación multicamino***: Ocurre cuando partes de la onda electromagnética se reflejan en los objetos y en el suelo. Esto genera que la señal sea menos limpia en el receptor.

Considerando lo anterior, se concluye que lo errores de bits serán más comunes en los enlaces inalámbricos.

Otra caso de estudio respecto a los errores de bits durante las transmisiones inalámbricas corresponde al problema del terminal oculto. Esto ocurre cuando uno de los receptores se encuentra obstruido. Otro escenario puede ocurrir debido al desvanecimiento de la intensidad de la señal

##### CDMA
*Completar si hay tiempo*
#### Capítulo 7.3
A lo largo de los años se han desarrollado una serie de estándares para las redes LAN, sin embargo la que ha perdurado hasta el día de hoy corresponde a la red LAN inalámbrica IEEE 802.11; mejor conocida como red WIFI. Existen varios estándares 802.11, los cuales se presentan a continuación:
- 802.11b
- 802.11a
- 802.11g
- 802.11n
- 802.11ac

Los dispositivos 802.11 operan en dos rangos de frecuencia distintos 2.4-2.4835 [GHz] y 5.1-5.8 [GHz]. La primera corresponde a una banda de frecuencia sin licencia, mientras que la segunda proporciona una distancia de transmisión más corta para determinado nivel de potencia y se ven más afectadas por la propagación multicamino.
##### La arquitectura 802.11
El componente fundamental de la arquitectura 802.11 es el BSS(Basic Service Set). Cada uno de estos contiene una o más estaciones inalámbricas y una estación base central(AP, acces point). En una red doméstica existirá un punto de acceso y un router que conectarán el BSS con internet. Tanto cada estación inalámbrica como punto de acceso tendrán una dirección MAC de 6 bytes.
También es posible agrupar las estaciones IEEE 802.11 para formar una red ad hoc; red sin control central y sin conexiones con el mundo exterior.
###### Canales y asociación
Una situación interesante es la de la *jungla WiFi*. Esta es cualquier ubicación física en la que una estación inalámbrica está recibiendo una señal suficientemente intensa desde dos o más puntos de acceso. Esto implica que el dispositivo inalámbrico se deberá conectar a un punto de acceso en concreto y que además este debe saber que puntos de acceso hay en la jungla.

El estándar 802.11 requiere que un punto de acceso envíe de forma periódica *tramas baliza*(beacon frames), que incluyan la dirección MAC y el identificador SSID del punto de acceso. Este estándar no especifica un algoritmo para seleccionar con cuál de los puntos de acceso disponibles asociarse.

El proceso de exploración de los canales y de escucha de las tramas baliza se conoce con el nombre de *exploración pasiva*. Un dispositivo inalámbrico también puede realizar una exploración activa, difundiendo una trama de sondeo que será recibida por todos los puntos de acceso dentro de su alcance. A continuación se describen ambos procesos.
- ***Exploración pasiva***:
1. Tramas baliza enviadas desde los AP.
2. Envío de la trama de solicitud de asociación desde el dispositivo inalámbrico al AP seleccionado.
3. Envío de la trama de respuesta de asociación desde el AP seleccionado al dispositivo inalámbrico.

- ***Exploración activa***
1. Difusión desde el dispositivo inalámbrico de una trama de solicitud de sondeo.
2. Envío de tramas de respuesta de sondeo desde las AP.
3. Envío de trama de solicitud de asociación desde el dispositivo inalámbrico al AP seleccionado.
4. Envío de trama de respuesta de asociación desde el AP seleccionado al dispositivo inalámbrico.

###### El protocolo MAC 802.11
Una vez conectados los dispositivos inalámbricos a los puntos de acceso es posible empezar a enviar y recibir datos. Para esto es necesario utilizar un protocolo de acceso múltiple para coordinar esas transmisiones. De forma general existen tres clases de protocolos de acceso múltiple:
- Particionamiento del canal
- Acceso aleatorio
- Toma por turnos

Para la arquitectura 802.11 se escogió un protocolo de acceso aleatorio para las redes LAN inalámbricas 802.11. Este se conoce como CSMA con evitación de colisiones(CSMA/CA, Collision Avoidance). en este cada estación sondeaa el canal antes de transmitir y se abstiene de transmitir cuando detecta que el canal está ocupado. Hay dos diferencias clave entre los protocolos MAC de Ethernet y de 802.11.
1. 802.11 utiliza técnicas de evitación de las colisiones
2. Utiliza un esquema de reconocimiento/retransmisión (ARQ) de la capa de enlace

El protocolo MAC 802.11 no implementa ningún mecanismo de detección de colisiones. Razones:
1. Poder detectar errores requiere la capacida de enviar y recibir datos al mismo tiempo.
2. Los problemas del terminal oculto y del desvanecimiento.

La descripción del protocolo CSMA/CA 802.11 se presenta a continuación:

1. Si la estación detecta inicialmente que el canal está inactivo, transmite la trama después de un corto periodo de tiempo (Espacio distribuido entre tramas , DIFS)
2. En caso contrario, la estación selecciona un valor de espera(backoff) aleatorio y efectúa una cuenta atrás con este valor desde DIFS mientras detecta que el canal está inactivo. Cuando detecta que el canal está ocupado, el valor del contador permanece congelado.
3. Cuando el contador alcanza el valor cero, la estación transmite la trama completa y luego esperaa recibir un reconocimiento.
4.  Si se recibe una trama de reconocimiento, la estación transmisora sabe que su trama ha sido recibida correctamente en la estación de destino. Si la estación tiene otra trama que enviar, comienza de nuevo el protocoloen el paso 2. Si no recibe una trama de reconocimiento, la estación transmisora vuelve a entrar en la fase de backoff del paso 2, seleccionando el valor aleatorio de un intervalo más largo.

####### Enfrentándose al problema de los terminales ocultos: RTS y CTS
EL protocolo MAC 802.11 también incluye un esquema de reserva que ayuda a evitar colisiones, incluso en presencia de terminales ocultos. Este permite a una estación utilizar una corta trama de *solicitud de transmisión*(RTS, request to Send) y otra corta trama de control de *preparado para enviar*(CTS, Clear to Send) para reservar el acceso al canal.
Cuando un emisor quiere enviar una trama:

1. Puede enviar una trama RTS al punto de acceso, indicando el tiempo total requerido para transmitir la trama y la trama de reconocimiento(ACK).
2. Cuando el punto de acceso recibe la trama RTS, responde difundiendo una trama CTS.

La trama sirve tanto para informar al emisor que puede enviar la información, como también para que otros emisores se abstengan de enviar su información.

El uso de estas tramas puede mejorar el rendimiento en dos formas:

- EL problema de las estaciones ocultas queda mitigado.

- Dado que ambas tramas son cortas, una colisión que implique a una de estas solo durará mientras duren esas tramas cortas RTS o CTS.

Aunque el intercambio RTS/CTS puede ayudar a reducir las colisiones, implica un mayor retardo y la utilización de más recursos del canal.


#### Capítulo 7.4 Redes celulares: 4G y 5G
#### Capítulo 7.5 Manejo de la movilidad: principios

Gran problema cuando existen distintos grados de movilidad. Se debe considerar desde casos estáticos hasta cuando un celular se mueve de un país a otro.

##### Estrategias de movilidad
Una de las estrategias que podrían pensarse es que la red misma lo maneje.
* Los routers deberían informar vía intercambio de tablas de ruteo la información de los nodos móviles.
* Ruteo de Internet podría realizarlo sin cambios.

***El gran problema es que no escala a billones de dispositivos.***

Por lo cual se utiliza una estrategia basada en los sistemas extremos. Funcionalidad en el borde.
* Ruteo indirecto: comunicación desde llamador al móvil a través de la red hogar y luego reenviada al móvil remoto.
* Ruteo directo: Llamador obitene dirección foránea del móvil y envía comunicación directamente al móvil.

**Home es una fuente de información sobre el dispositivo, así la gente puede preguntar a dicho lugar sobre ti.**

La home network, o home mobile carrier network, es aquel que nos provee del servicio pagado, por ejemplo Entel. Dentro de ella existe el HSS, el cual contiene la información sobre nosotros y los servicios disponibles según el caso.

Por otro lado la visited network, o visited mobile network, es cualquier red distinta al home network. Podría haber un acuerdo de servicio para proveer al dispositivo, por ejemplo el roaming.

Para ISP/WiFi no existe la noción de "home" global. Para redes diferentes hay credenciales diferentes, salvo la red eduroam (educational roaming WiFi). Existe una arquitectura para movilidad (mobile IP) pero no es usada, ha sido "opacada" por otras soluciones.

---

##### Registro.

Se debe saber dónde está el móvil al momento del llamado. Por lo cual:

1. El móvil se asocia con el mobility manager.
2. Mobility manager visitado registra localización del móvil en su HSS.

Para la movilidad con ruteo indirecto...

1. El corresponsal usa dirección home como destino del datagrama.
2. Luego el home gateway reenvia al gateway remoto (tunneling).
3. El gateway router visitado reenvía datagrama al móvil.
4. Gateway del router visitado reenvía respuestas al corresponsal directamente o al home network para que el lo reenvie.

**Es ineficiente, es un ruteo triangular.**

Para la movilidad con ruteo directo...

1. Corresponsal contacta con HSS.
2. Obtiene red visitada por el móvil.
3. Corresponsal manda directamente a la red visitada.
4. Gateway router visitado reenvía el datagrama al móvil.

***A diferencia del indirecto, no es transparente para el corresponsal. ¿Qué pasa cuando se cambia nuevamente de red?***

#### Capítulo 7.6 Manejo de la movilidad en la práctica
##### Manejo de mobilidad en redes 4G/5G

1. El móvil se asocia a la estación proveyendo su IMSI.
2. Configuración del plano de control. MME y HSS establecen el estado del plano de control.
3. Configuración del plano de datos. El MME configura los túneles de reenvío para el móvil. Mientras las redes visitadas y hogar establecen túneles desde P-GW a móvil.
4. Handover del móvil. Es donde el dispositivo móvil cambia su punto de conexión a red visitada.

![mobilidad](Imagenes/mobilidad4g.png "mobilidad")

---

Para el plano de control en LTE, el móvil se comunica con el MME local. Luego el MME usa la información del IMSI para contactar al HSS hogar y obtener más información. Finalmente la BS y el móvil seleccionan parámetros para el plano de datos.

---

##### Configuración de túneles del plano de datos del móvil

* Túnel S-GW a BS. Cuando un móvil cambia de BS, simplemente cambia la dirección IP de un extremo del túnel.
* Túnel S-GW a P-GW hogar. Implementando un ruteo indirecto.
* Tunelización vía GTP. Un datagrama del móvil es encapsulado usando GTP dentro de UDP.

---

##### Handovers entre BSs en la misma red

1. La BS actual selecciona la BS de destino, envía un *request message*.
2. BS de destino responde con HR ACK.
3. BS informa al movil sobre la nueva BS.
4. BS fuente reenvía los datagramas a la BS nueva, para que ella los envíe al móvil.
5. BS destino informa a MME que es nueva BS para el móvil, además el MME le dice al S-GW que cambie su túnel.
6. BS destino envía ACK a la BS fuente, el handover fue completo.
7. Los datagramas del móvil van a la BS de destino mediante el S-GW.

##### Mobilidad IP
Definido bajo el [RFC5944](https://www.rfc-editor.org/rfc/rfc5944.html) para IPv4. Posee múltiples modos de operar, es un protocolo flexible y complejo, muy parecido a las redes 4G/5G. Se puede separar en 3 piezas.

- Agent Discovery. El nodo de IP móbil debe aprender la identidad del agente. Dicho proceso se lleva a cabo mediante: *agent advertisement* o *agent solicitation*. Información del servicio:
  - CoA
  - HA address
  - MA address
  - Lifetime 9999
  - Identification
- Registration with the home agent. Se registre o se desregistra los CoAs con un agente home móbil.
- Indirect routing of datagrams. Indica cómo rutear y enviar los datagramas, sus reglas y formatos. Bajo los [RFC2003](https://www.rfc-editor.org/rfc/rfc2003) y [RFC2004](https://www.rfc-editor.org/rfc/rfc2004).

![comparacion](Imagenes/comparacion.png "comparacion")

#### Capítulo 7.7 Mobilidad e inalambrico: Impactos en los protocolos de capas altas

El impacto es mucho mayor de lo que se pensaría, principalmente en el desempeño.

* Pérdidas y retardo de paquetes debido a errores en bits y pérdidas por **handover**.
* TCP interpreta pérdidas como congestión, decrementa ventana de congestión innecesariamente.
* Retardo impide tráfico de tiempo real.
* El ancho de banda es un recurso **escaso** en enlaces inalámbricos.

### Glosario

1. IMSI: International Mobile Suscriber Identity.
2. MME: Mobility Management Entity.
3. ICMP: Internet Control Message Protocol. [RFC792](https://www.rfc-editor.org/rfc/rfc792)
4. CoA: Care-of-Address
5. GSM: Global System for Mobile Communications
6. Home PLMN: Home Public Land Mobile Network = Home Network
7. Visited PLMN: Visited Public Land Mobile Network = Visited Network
8. HLR: Home Location Register
9. VLR: Visitor Location Register
10. GMSC: Gateway Mobile Services Switching Center
11. MSRN: Mobile Station Roaming Number
12. HSS: Home Suscriber Service
13. PDN: Package Data Network
14. P-GW: PDN (&uarr;) Gateway. Mapeo nat, direccionamiento de la red de la empresa. Crea túneles.
15. S-GW: Serving Gateway. Hace túneles, provee funciones de movilidad cuando los equipos desean moverse.
16. GTP: GPRS Tunneling Protocol