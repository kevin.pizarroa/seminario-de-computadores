## HOW TO SPEAK - PATRICK WINSTON

Analogía de no dejar a un soldado a la batalla sin un arma y no dejar a un estudiante salir a la vida sin la habilidad para comunicarse.

Lo que importa...
1. Speak about your ideas.
2. Write about your ideas.
3. Quality of your ideas.

Quality of Speaking =  integrate ( function(Knowledge_huge, Pratice_medium, talent_small)))


We have only 1 language processor.

### How to Start:
- No Jokes.
- Promises.

- Use samples.
- Cycle the topics.
- Build fence. Que la gente no se confunda con las ideas.
- Verbal punctuation. 
- Ask a Question. No puede ser ni muy obvia ni muy compleja!

***DEVELOP YOUR OWN STYLE***

**TIME**: 11 am 
**PLACE**: Well lit, cased, reasonabily populated.

**PROPS**: la tiza o un marcador puede ser más efectivo cuando quieres informar o enseñar, slides es más efectivo cuando quieres exponer.

**BOARD**:
- Graphic
- Speed
- Target

***HANDS ARE PUBLIC!***

Empathetic Mirroring -> Neuronas espejo: Te ves a ti mismo escribiendo en el pizarrón.


***Too many words too many slides.***

### Rules:
1. Dont read
2. Be in the image
3. Keep Images simple
4. Eliminate Clustter
5. No logos
6.  No titulos. Obliga a las personas a leer esto -> No ponen atención al orador.
7. Letras de 40 pts de tamaño.
8. NO usar puntero -> se pierde el contacto visual.
9. Not Too heavy.
10. MOSTLY PICTURES

***Exhibited passion about what they were doing***

### What is needed for job talks?
- Vision (Problem & approach)
- Done Something (Steps to reach it, constraints or anything else) 
- Contributions

**Expose that in 5 minutes.** Otherway, you're done.



### Getting famous?
Why should I care about that?
- Te puedes acostumbrar a ser famoso, pero no te puedes acostumbrar a ser ignorado.
#### How to?
- Śymbol (That can be related to your work), slogan (one shot), surprise, salient idea (not necessarly important) near to miss & story about your work.

### How to Stop?
#### Final slides
Should be -> Contributions of your work instead of "Questions" "Thank you"

#### Final ideas
- Joke
- Thank you (is really weak, not recommended)
- Bendecir (?) Nope -> ITE MISSA ET.
- Salute the audience. (Ha sido un gran día, llena de buenas preguntas y espero volver.)


---
## HOW TO GIVE A GREAT RESEARCH TALK - SIMON PEYTON JONES

### Research is communication!

Empieza presentando por qué las charlas son útiles y por qué se debería saber cómo dar buenas charlas.

#### What is the purpose of your talk?
- Is not to impress, is not to tell them everything you know about your topic and is not to present all the technical details. 
-  Is to give and intuitive feel for your idea. Engage to read your paper. Excite and provoke them. Make them glad to went to your talk.

**NOTE: The audience you get probably never heart of you, just had a lunch and are ready for a doze.**

Your mission is to WAKE THEM UP.


### What to put in your talk?
#### 20% Motivation about your idea (around 2 minutes)
- What's the problem and why is interesting?
- Why should I tune to this talk?

#### 80% The key idea (around 8 minutes). If the audience remembers only one things from your talk, what should it be?
- Be specific, absolutely.
- Organize your talk around this specific goal
- It's ok to cover only part of your paper.
- Examples are your MAIN WEAPON.
- "Cómo aprendemos? de lo específico a lo general mientras que usualmente se enseña de lo
general hacia lo específico."

#### WHAT TO LEAVE OUT?
1. Background
2. FLUGOL
3. Benchmark result
4. Related Work
5. Conclusions and further work
6. Technical details

#### PRESENTING YOUR TALK
- Your most precious weapon is your enthusiasm!
- Write your slides the night before. (MUST BE FRESH)
- Don't apologize. The audience can't do anything.
- JELLY EFFECT -> BREATH, MOVE AROUND, USE LARGE GESTURES & SCRIPT YOUR FIRST FEW SENTENCES PRECISELY.

- Don't start with jokes.
- Face the audience, not the screen
- Know your material
- Laptop in front, screen back.
- Speak to the nodder (somnoliento)
- Better to connect and not to present all your material.
- Don't use animated slides.
- Don't say would you like me to go on? 
- When your time is up continuing is very counter productiv e-o> truncate and conclude.

---
## HOW TO GIVE A GOOD SCIENTIFIC TALK - JULIAN RAYNER

1.	You know way more about your subject than anyone else. Never assume the knowledge of the audience.
2. Practice is everything. Try to present to people who knows nothing.
3. EYE CONTACT!
4. Keep it simple and clean. DESIGN. Otherwise the audience will get distracted.
5. 1 slide / minute.
6. Minimise stress.
7. Go to the podium before the talk. You must know the equipment and get used to the space.
8. Be aware of your audience -> ADAPT
9. Passion and get excited!
10. Answer & question &rarr Listen before answer. &rarr Answer what is asked.