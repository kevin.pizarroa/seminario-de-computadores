<script
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
  type="text/javascript">
</script>

## C1 - Resumen: Redes de Computadores: Un enfoque descendente.
### [Capítulo 7 Redes Inalámbricas y Móviles. 8va edición](Apuntes/Cap7.md)
### [Capítulo 9 Multimedia en Redes de Computadores. 7ma edición](Apuntes/Cap9.md)

---

### Sobre investigación y otros recursos
#### [Resumen charla "You and your research" de Richard Hamming](Apuntes/Ideas - you and your research.md)
#### [Links de utilidad](Apuntes/links utiles.md)